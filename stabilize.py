import utils
import video_stabilization as vs


def main():

  frameList = utils.readVideo('Output/andreamble/colortest.mp4')
  frameList = vs.stabilize(frameList)

  utils.writeVideo('Output/andreamble/stabilized.mov', frameList)

if __name__ == '__main__':
  main()
